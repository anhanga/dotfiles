#
# ~/.bash_profile
#

# Add ~/.local/bin to PATH
home_bin=$HOME/.local/bin
[[ -d $home_bin && :$PATH: != *:$home_bin:* ]] && PATH=$home_bin:$PATH

export EDITOR=kak

export XDG_CONFIG_HOME=~/.config
export XDG_CACHE_HOME=~/.cache
export XDG_DATA_HOME=~/.local/share
export XDG_STATE_HOME=~/.local/state

[[ $(hostname) = x220 ]] && {
    export GDK_SCALE=0.9
    export GDK_DPI_SCALE=0.9
}

# Autostart graphical session on login
if [[ $(tty) = /dev/tty1 ]]; then
    pgrep awesome || startx
fi

. ~/.bashrc
