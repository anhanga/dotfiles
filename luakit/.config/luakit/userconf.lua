--- userconf.lua

local modes = require "modes"
local settings = require "settings"

-- Adblock
local adblock = require "adblock"
local adblock_chrome = require "adblock_chrome"

-- Search engines
local engines = settings.window.search_engines
engines.aw = "https://wiki.archlinux.org/index.php/Special:Search?fulltext=Search&search=%s"
engines.gw = "https://wiki.gentoo.org/index.php?title=Special%ASearch&search=%s"
engines.ddg = "https://duckduckgo.com/?q=%s"
engines.sx = "https://searx.tiekoetter.com/?q=%s"
engines.default = engines.sx

-- Change select labels
local select = require "select"
select.label_maker = function ()
    local chars = charset("asdfhjkl")
    return trim(sort(reverse(chars)))
end

local follow = require "follow"
follow.pattern_maker = follow.pattern_styles.match_label
